presentations = [
{title: "Yellow presentation",
img: "y1.jpg",
data: ["y1.jpg","y2.jpg","y3.jpg","y4.jpg","y5.jpg","y6.jpg","y7.jpg"]
},
{title: "Green presentation",
img: "g5.jpg",
data: ["g5.jpg","g6.jpg","g7.jpg"]
},
{title: "Flowers",
img: "flower1.jpg",
data: ["flower1.jpg","flower2.jpg","flower3.jpg","flower4.jpg"]
}]

function Presentation(data)
{
this.data=data
this.curpos=0;
}

Presentation.prototype.next=function(){
if (this.curpos != this.data.length-1) this.curpos+=1;
else this.curpos=0;
$("#sl_"+this.curpos).click();
}

Presentation.prototype.prev=function(){
if (this.curpos != 0) this.curpos-=1;
else this.curpos=this.data.length-1;
$("#sl_"+this.curpos).click();
}

Presentation.prototype.slide=function(num){
this.curpos=num;
return this.data[this.curpos];
}


function startPresentation(slides){
prs=new Presentation(slides)
$("#film").empty();
var count=slides.length;
for(var i=0;i<count;i++){
$("#film").append("<div id=sl_"+i+">")
var div=$("#film").find("div").last()
div.append("<img>");
div.find("img").last().attr("src","slides/"+slides[i]);
div.click(function() {
$("#slide").find("img").attr("src","slides/"+prs.slide(+this.id.split("_")[1]));
});
}
$("#slide").find("img").attr("src","slides/"+prs.slide(0));
$("#slide").click(function(){
prs.next();
})
$("#presentation").show();
}

$(document).ready(function(){ 

$(document).keyup(function(eventObject){
  if(prs=="undefined") return
  if(eventObject.keyCode == 39) prs.next();
  if(eventObject.keyCode == 37) prs.prev();
  if(eventObject.keyCode == 27) $("#presentation").hide();
});

var count=presentations.length;

for(var i=0;i<count;i++){
$("#presentationslist").append("<div id=pr_"+i+" class='tumbnail'>")
var div=$("#presentationslist").find("div").last()
div.append("<img>");
div.find("img").last().attr("src","slides/"+presentations[i].img);
div.append("<p>")
div.find("p").last().text(presentations[i].title);

div.click(function() {
var index=+$(this).closest("div").attr("id").split("_")[1]

startPresentation(presentations[index].data)
});
}
});